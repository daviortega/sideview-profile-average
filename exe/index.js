#!/usr/bin/env node
/* eslint-disable no-console*/
'use strict'

const ArgumentParser = require('argparse').ArgumentParser
const fs = require('fs')
const figlet = require('figlet')
const chalk = require('chalk')

const pkjson = require('../package.json')

const splash = figlet.textSync('sideview-profile-average', {horizontalLayout: 'fitted'})
console.log(chalk.cyan(splash))
console.log(`\t\t\t\t\t\t\t            ${chalk.cyan('' + pkjson.version)}`)
console.log(chalk.red('\t\t\t\t\t\t\t\t\t\t\t   by Davi Ortega'))

const sideViewAverage = require('../src/sideViewAverageProfile')

let parser = new ArgumentParser({
	addHelp: true,
	description: 'Calculates the 1D vpxel intensity profile average in sideviews following a path (countour)'
})

parser.addArgument(
	['-t', '--tif-file'],
	{
		help: 'TIF formated file of the tomogram stack'
	},
)

parser.addArgument(
	['-m', '--model-file'],
	{
		help: 'Text formated file of model points'
	},
)

parser.addArgument(
	['-p', '--parameters'],
	{
		help: 'stepsize(1) profile-length(100) depth(10). Integer values separated by spaces that indicates, in order, the size of the step, the length of the profile and the depth of the averaged stack. In parenthesis are recommended values.',
		nargs: 3
	},
)

let args = parser.parseArgs()
const modelFileName = args.model_file.replace(/\.([a-z]|[A-Z]){2,4}$/,'')
const output = args.tif_file.replace(/\.([a-z]|[A-Z]){2,4}$/, `.${modelFileName}.${args.parameters.join('.')}.json`)

sideViewAverage(args.tif_file, args.model_file, args.parameters).then((result) => {
	fs.writeFileSync(output, JSON.stringify(result))
})

