/* eslint-disable no-magic-numbers */
'use strict'
const loadModule = require('./loadModel')
const {expect} = require('chai')

describe('loadModule', () => {
	it('should work', () => {
		const filename = './test-data/testTomogramModel.dat'
		const rawModel = loadModule(filename)
		const expectedRawModel = {
			refPath: [[32, 31, 48], [51, 52, 48], [70, 73, 48]],
			refVec: [-34, 11, 0]
		}
		expect(rawModel).eql(expectedRawModel)
	})
	it('should throw meaningful error', () => {
		const filename = './test-data/badModel.dat'
		expect(() => loadModule(filename)).throw('It appears that the reference vector was passed with more than 2 points. Perhaps the you switched the path and the reference vector - the path must be the first countour and the reference vector the second.')
	})
})
