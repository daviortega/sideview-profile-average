'use strict'
const Logger = require('loglevel-colored-prefix')
const fs = require('fs')

const logger = new Logger.Logger('INFO')

const kDefaults = {
	entryLength: 5,
	contourPos: 1
}

const subArrays = (vec1, vec2) => {
	const log = logger.getLogger('loadModule::subArrays')
	log.debug(vec1)
	log.debug(vec2)
	const result = []
	vec1.forEach((x, i) => {
		result.push(x - vec2[i])
	})
	return result
}

module.exports = (filename) => {
	const log = logger.getLogger('loadModule::main')
	const data = fs.readFileSync(filename).toString()
	const lines = data.split('\n')
	const coords = []
	lines.forEach((line) => {
		line.split(' ').filter((v) => v !== '')
			.forEach((value) => {
				log.debug(`---${value}---`)
				coords.push(parseInt(value))
			})
	})
	const refPath = []
	let refVec = []
	log.debug(coords)
	let countRefVecPoints = 0
	while (coords.length > 0) {
		const entry = coords.splice(0, kDefaults.entryLength)
		log.debug(entry)
		if (countRefVecPoints > 1 && entry[kDefaults.contourPos] === 1)
			throw new Error('It appears that the reference vector was passed with more than 2 points. Perhaps the you switched the path and the reference vector - the path must be the first countour and the reference vector the second.')
		if (entry[kDefaults.contourPos] === 0) {
			refPath.push(entry.slice(-3).map((a) => a - 1))
		}
		else if (refVec.length === 0) {
			refVec.push(entry.slice(-3).map((a) => a - 1))
			countRefVecPoints++
		}
		else {
			refVec = subArrays(refVec[0], entry.slice(-3).map((a) => a - 1))
			countRefVecPoints++
		}
	}

	log.info(refPath)
	log.info(refVec)

	return {
		refPath,
		refVec
	}
}
