'use strict'

const ndarray = require('ndarray')
const zeros = require('zeros')
const pool = require('ndarray-scratch')
const ops = require('ndarray-ops')
const gemm = require('ndarray-gemm')

const arraysEqual = (arr1, arr2) => {
	if (arr1.length !== arr2.length)
		return false
	for (let i = arr1.length; i--;) {
		if (arr1[i] !== arr2[i])
			return false
	}
	return true
}

const crossProd = (a, b) => {
	if (!arraysEqual(a.shape, b.shape))
		throw Error('Vectors must have same dimensions')
	const s1 = a.get(0, 1) * b.get(0, 2) - a.get(0, 2) * b.get(0, 1)
	const s2 = a.get(0, 2) * b.get(0, 0) - a.get(0, 0) * b.get(0, 2)
	const s3 = a.get(0, 0) * b.get(0, 1) - a.get(0, 1) * b.get(0, 0)
	return ndarray([s1, s2, s3], [1, 3])
}

/**
 * Returns the vector perpendicular to the first input vector in the same plane defined
 * by the first and second input vectors.
 *
 * @param {View3dfloat64} vec1
 * @param {View3dfloat64} vec2
 * @returns {View3dfloat64} perpVec
 */

const perpUnitVecSamePlane = (vec1, vec2) => {
	const a = pool.clone(vec1)
	const b = pool.clone(vec2)
	const scalar = zeros([1, 1])
	const perpVec = zeros(a.shape)

	gemm(scalar, a, b.transpose(1, 0))
	ops.mulseq(a, scalar.get(0, 0))
	ops.sub(perpVec, b, a)

	const modPerpVec = ops.norm2(perpVec)
	ops.divseq(perpVec, modPerpVec)
	return perpVec
}

module.exports = {
	crossProd,
	perpUnitVecSamePlane
}
