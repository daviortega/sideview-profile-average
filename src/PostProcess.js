'use strict'

const fs = require('fs')
const pack = require('ndarray-pack')
const unpack = require('ndarray-unpack')
const norm = require('ndarray-normalize')
const pool = require('ndarray-scratch')

module.exports =
class PostProcess {
	constructor(profiles) {
		this.profiles = profiles
	}

	parseProfiles() {
		this.parsed = []
		this.profiles.forEach((profile, i) => {
			profile.forEach((dataPoint, j) => {
				this.parsed.push({
					x: j,
					y: parseFloat(dataPoint),
					profile: i
				})
			})
		})
		console.log('profiles parsed')
		return this
	}

	getMinMax() {
		const parsedProfiles = this.parsed || this.profiles
		const maxValue = Math.max(...parsedProfiles.map((a) => a.y))
		const minValue = Math.min(...parsedProfiles.map((a) => a.y))
		return {maxValue, minValue}
	}

	globalNorm() {
		this.gNorm = [] // pool.clone(this.profiles)
		const minMax = this.getMinMax()
		this.parsed.forEach((value) => {
		  const newValue = JSON.parse(JSON.stringify(value))
		  newValue.norm = ((newValue.y - minMax.minValue) / (minMax.maxValue - minMax.minValue))
		  this.gNorm.push(newValue)
		})
		console.log('global Norm done')
		return this
	}

	calcAvgProfile(normThis = true) {
		const parsedProfiles = normThis ? this.gNorm : this.parsed
		this.average = {}
		if (parsedProfiles) {
			const result = []
			const resultNorm = []
			const profiles = new Set()
			parsedProfiles.forEach((dataPoint) => {
			  profiles.add(dataPoint.profile)
			  while (dataPoint.x > result.length - 1) {
					result.push(0)
					resultNorm.push(0)
			  }
			  result[dataPoint.x] += dataPoint.y
			  resultNorm[dataPoint.x] += dataPoint.norm
			})
			const numProfiles = profiles.size
			for (let i = 0; i < result.length; i++) {
			  result[i] /= numProfiles
			  resultNorm[i] /= numProfiles
			}
			this.average.stats = {
				mean: result.reduce((a, b) => a + b) / result.length
			}
			this.average.stats = {
				meanNorm: resultNorm.reduce((a, b) => a + b) / resultNorm.length
			}
			this.average.data = result
			this.average.dataNorm = resultNorm
			return this
		}

		throw Error('Must parse data first.')
	}

	getData(options = {
		rawData: true,
		parsed: false,
		gNorm: false,
		average: true
	}) {
		const wrap = {}
		if (options.rawData)
			wrap.rawData = this.profiles

		if (options.parsed)
			wrap.parsed = this.parsed

		if (options.gNorm)
			wrap.gNorm = this.gNorm

		if (options.average)
			wrap.average = this.average
		return wrap
	}
}


