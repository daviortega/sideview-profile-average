'use strict'

const expect = require('chai').expect

const ndarray = require('ndarray')
const utils = require('./utils')

describe('utils', function() {
	describe('crossProd', function() {
		it('should give the right answer', function() {
			const a = ndarray(new Float32Array([1, 0, 0]), [1, 3])
			const b = ndarray(new Float32Array([0, 1, 0]), [1, 3])
			const expected = ndarray([0, 0, 1], [1, 3])
			const dotP = utils.crossProd(a, b)
			expect(dotP).eql(expected)
		})
		it('should give the right answer', function() {
			const a = ndarray(new Float32Array([-1, 7, 4]), [1, 3])
			const b = ndarray(new Float32Array([-5, 8, 4]), [1, 3])
			const expected = ndarray([-4, -16, 27], [1, 3])
			const dotP = utils.crossProd(a, b)
			expect(dotP).eql(expected)
		})
	})
	describe('perpUnitVecSamePlane', function() {
		it('should give the right answer', function() {
			const a = ndarray(new Float32Array([1, 0, 0]), [1, 3])
			const b = ndarray(new Float32Array([1, 1, 0]), [1, 3])
			const expected = ndarray(new Float64Array([0, 1, 0]), [1, 3])
			const perpVec = utils.perpUnitVecSamePlane(a, b)
			expect(perpVec).eql(expected)
		})
	})
	describe.skip('dotProd - not used anymore', function() {
		it('should give zero for orthonormal arrays', function() {
			const a = ndarray(new Float32Array([1, 0]), [1, 2])
			const b = ndarray(new Float32Array([0, 1]), [2, 1])
			const expected = 0
			const dotP = utils.dotProd(a, b)
			expect(dotP).eql(expected)
		})
		it('should give 1 for same array', function() {
			const a = ndarray(new Float32Array([1, 0]), [1, 2])
			const b = ndarray(new Float32Array([1, 0]), [2, 1])
			const expected = 1
			const dotP = utils.dotProd(a, b)
			expect(dotP).eql(expected)
		})
		it('should give 1 for [1, 0] dot [1, 1]', function() {
			const a = ndarray(new Float32Array([1, 0]), [1, 2])
			const b = ndarray(new Float32Array([1, 1]), [2, 1])
			const expected = 1
			const dotP = utils.dotProd(a, b)
			expect(dotP).eql(expected)
		})
		it('should give 32 for [1, 2, 3] dot [1, 5, 7]', function() {
			const a = ndarray(new Float32Array([1, 2, 3]), [1, 3])
			const b = ndarray(new Float32Array([1, 5, 7]), [3, 1])
			const expected = 32
			const dotP = utils.dotProd(a, b)
			expect(dotP).eql(expected)
		})
		it('should give zero for orthonormal arrays', function() {
			const a = ndarray(new Float32Array([-0.392, 0.608, 0]), [1, 3])
			const b = ndarray(new Float32Array([0.392, -0.608, 0]), [3, 1])
			const expected = 0
			const dotP = utils.dotProd(a, b)
			expect(dotP).eql(expected)
		})
	})
})
