'use strict'
const Logger = require('loglevel-colored-prefix')

const sharp = require('sharp')
const ndarray = require('ndarray')
const show = require('ndarray-show')
const ops = require('ndarray-ops')
const zeros = require('zeros')
const pool = require('ndarray-scratch')
const tests = require('ndarray-tests')
const unpack = require('ndarray-unpack')
const rot = require('image-rotate')
const prefixSum = require('ndarray-prefix-sum')

const loadModel = require('./loadModel')
const utils = require('./utils')
const PostProcess = require('./PostProcess')


const logger = new Logger.Logger('INFO')


module.exports = (tomogramFile, modelFile, parameters = [1, 100, 10]) => {
    const logMain = logger.getLogger('sideViewAverageProfile::main')
    const [stepSize, profileLength, depth] = parameters
    const halfProfileLength = Math.floor(profileLength/2)
    const halfDepth = Math.floor(depth/2)

    const flatten = (ary) => ary.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), [])

    const loadOnePageTiff = (filename, page) => {
        const log = logger.getLogger('sideViewAverageProfile::loadOnePageTiff')
        return sharp(filename, {page})
            .toColourspace('b-w')
            .raw()
            .toBuffer({resolveWithObject: true})
            .then((data) => {
                log.debug(data.info)
                return data 
            })
            .catch((err) => {
                log.error(err)
                throw err
            })
    }

    const loadMultiPageTiff = async (filename, pages) => {
        const log = logger.getLogger('sideViewAverageProfile::loadMultiPageTiff')
        const pixelValues = {
            data: {}
        }
        for (let i = 0, N = pages.length; i < N; i++) {
            log.info(`Page: ${pages[i]}`)
            await loadOnePageTiff(filename, pages[i]).then((data) => {
                if (data) {
                    if (!i)
                        pixelValues.info = data.info
                    if (JSON.stringify(data.info) === JSON.stringify(pixelValues.info)) {
                        log.debug(pixelValues.data)
                        const rawSlice = ndarray(new Uint8Array(data.data.toJSON().data), [data.info.height, data.info.width, data.info.channels]).pick(null, null, 0)
                        const test = pool.clone(rawSlice)
                        prefixSum(test)
                        const totalPix = test.get(0, test.shape[0])
                        log.debug(`total pixelvalue in page: ${totalPix}`)
                        const rotSlice = zeros([rawSlice.shape[1], rawSlice.shape[0]])
                        rot(rotSlice, rawSlice, -Math.PI/2)
                        pixelValues.data[pages[i]] = rotSlice
                    }
                    else { 
                        log.info(data.info)
                        log.info('***')
                        log.info(pixelValues.info)
                        throw Error('Yo... there is something weird with the metadata')
                    }
                }
            })
        }

        if (!pixelValues.info) {
            const errMessage = 'Something went wrong while loading the tif file.'
            log.error(errMessage)
            throw new Error(errMessage)
        }
        log.info(pixelValues.info)
        log.info('we are done loading this stuff')
        return pixelValues.data
    }

    const coordsRaw = loadModel(modelFile)
    const coords = ndarray(new Int16Array(flatten(coordsRaw.refPath)), [coordsRaw.refPath.length, 3] )

    let refVec = ndarray(new Float32Array(coordsRaw.refVec), [1, 3])
    let refUnitVec = pool.clone(refVec)
    ops.divs(refUnitVec, refVec, ops.norm2(refVec))

    const listOfProfiles = []
    const samplePoint = zeros([1, 3])
    const listOfZslices = []

    for (let i = 0; i < coordsRaw.refPath.length - 1; i++) {
        const currPathPoint = zeros([1, 3])
        ops.assign(currPathPoint, coords.lo(i,0).hi(1,3))
        logMain.info(`---currPathPoint ${i}---`)
        logMain.info(show(currPathPoint))
        
        const pathUnitVec = zeros([1, 3])
        ops.sub(pathUnitVec, coords.lo(i+1,0).hi(1,3), coords.lo(i,0).hi(1,3))
        const modPathVec = ops.norm2(pathUnitVec)
        const numberOfSamplePointsInPath = Math.floor(modPathVec/stepSize)
        ops.divseq(pathUnitVec, modPathVec)
        logMain.info(`---pathUnitVec norm ${i}---`)
        logMain.info(show(pathUnitVec))

        const profileUnitVec = utils.perpUnitVecSamePlane(pathUnitVec, refUnitVec)
        logMain.info(`---profileUnitVec ${i}---`)
        logMain.info(show(profileUnitVec))

        const depthUnitVec = utils.crossProd(profileUnitVec, pathUnitVec)
        ops.mulseq(depthUnitVec, ops.norm2(depthUnitVec))
        logMain.info(`---depthUnitVec ${i}---`)
        logMain.info(show(depthUnitVec))

        if (!tests.vectorsAreOrthogonal(ndarray(pathUnitVec.data, [3]), ndarray(profileUnitVec.data, [3]), 1e-2))
            throw Error('Error calculating the direction of the profiles')


        const profileVec = zeros([1, 3])
        const depthVec = zeros([1, 3])
        const depthAvgPoints = []

        for (let j = 0; j < numberOfSamplePointsInPath; j++) {
            if (j) {
                const incrementPath = zeros([1, 3])
                ops.muls(incrementPath, pathUnitVec, stepSize)
                ops.addeq(currPathPoint, incrementPath)
            }
            logMain.debug('***path point****')
            logMain.debug(show(currPathPoint))
            const profile = []
            logMain.debug('    ***profile point****')
            for (let k = 0; k < profileLength; k++) {
                ops.muls(profileVec, profileUnitVec, halfProfileLength - k)
                ops.add(samplePoint, currPathPoint, profileVec)
                logMain.debug(`    ${show(samplePoint)}`)
                const depthToAvg = []
                logMain.debug('        ***depth points****')
                for (let l = 0; l < depth; l++) {
                    if (l)
                        ops.add(samplePoint, currPathPoint, profileVec)
                    ops.muls(depthVec, depthUnitVec, halfDepth - l)
                    ops.addeq(samplePoint, depthVec)
                    logMain.debug(`        ${show(samplePoint)}`)
                    const coord = unpack(samplePoint.pick(0)).map((coord) => Math.round(coord))
                    depthToAvg.push(coord)
                    if (listOfZslices.indexOf(coord[2]) === -1)
                        listOfZslices.push(coord[2])
                }
                logMain.debug('        ***END depth points****')
                profile.push(depthToAvg)
            }
            logMain.debug('    ***END profile point****')
            listOfProfiles.push(profile)
        }
        logMain.debug(`Sample points ->\n${show(samplePoint)}`)
        logMain.debug('*********************************')
    }

    logMain.info(listOfZslices)

    return loadMultiPageTiff(tomogramFile, listOfZslices).then((tiff) => {
        const listOfProfileValues = {
            rawData: []
        }
        logMain.debug(tiff)
        listOfProfiles.forEach((profile, i) => {
            const profileValues = []
            logMain.info(`Profile: ${i + 1} of ${listOfProfiles.length} `)
            profile.forEach((depthArray) => {
                let values = 0
                logMain.debug('New point ***')
                depthArray.forEach((coords) => {
                    const value = tiff[coords[2]].get(...coords.slice(0, 2))
                    logMain.debug(`${coords.map(a => a + 1)} - ${value}`)
                    values += value
                })
                values /= depthArray.length
                profileValues.push(values.toPrecision(5))
            })
            listOfProfileValues.rawData.push(profileValues)
        })
        const pp = new PostProcess(listOfProfileValues.rawData)
        const toBeSaved = pp.parseProfiles()
            .globalNorm()
            .calcAvgProfile()
            .getData({
                rawData: false,
                parsed: false,
                gNorm: true,
                average: true
            })
        logMain.info('All went well here')
        return toBeSaved // fs.writeFileSync(outfile, JSON.stringify(toBeSaved))
    }).catch((err) => {throw err})
}


