/* eslint-disable no-magic-numbers */
'use strict'

const fs = require('fs')
const path = require('path')
const expect = require('chai').expect

const sideViewProfileAverage = require('./sideViewAverageProfile')

const testTomogramFixture = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../test-data', 'testTomoFixture.json')).toString())
const testTomogramFile = path.resolve(__dirname, '../test-data', 'testTomogram.tif')
const testTomogramModelFile = path.resolve(__dirname, '../test-data', 'testTomogramModel.dat')
const testTomograParameters = [1, 50, 3]

const output = 'test.json'

describe('sideViewProfileAverage', function() {
	it('should work - only turn it on if it has the right tomogram (in tif)', function() {
		this.timeout(1000)
		return sideViewProfileAverage(testTomogramFile, testTomogramModelFile, testTomograParameters).then((results) => {
			expect(results).eql(testTomogramFixture)
		})
	})
})
