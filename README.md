# Making side view proflie averages from tomograms

The goal here is to take advantage of Imod to make 1D pixel intensity profiles average in sideviews following a specific path (countour).

For that we need a tomograms exported in multi-page tiff format, a Imod model file exported to points using `model2point`. This model file takes two contours:

1) Containing a path (Contour 0)
2) Containing a non parallel, straight line path (two dots)

The first is the path used to guide where the profile samples will be taken. These profile samples will be perpendicular to the first contour. The second will be used to define the plane where the 1D profile samples should be taken.

Finally, the user need to define the length of the profile, centered in the path, and the intervals (in pixels) in which that samples will be taken.

## Install

This application is a command-line application in `nodejs` and in this tutorial we will assume that the user is in a some sort of `unix` related environment.

### Install nodejs

The easiest way to install nodejs is by using `nvm` or [Node Version Manager](https://github.com/creationix/nvm).

From their github page you can install nvm by doing:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
```

you might need to close your terminal and reopen, but the next time you type `nvm` you should see a menu with options. Please refer to google if you have trouble to install `nvm`.

...and that was probably the hardest part, because now you can install ~~`node 9.10.1`~~ `12.18.2` with:

```bash
nvm install 12.18.2
```

and make it the default:

```bash
nvm alias default 12.18.2
```

You should be all set and if so, you will be able to do this:

```bash
$ node
> 2 + 2
4
>
```

Hit Ctrl-d to get out.

### Installing sideview-profile-average

Sideview-profile-average is on npm under the name [sideview-profile-average](https://www.npmjs.com/package/sideview-profile-average) and to install simply:

```bash
npm install -g sideview-profile-average
```

you should be all set. You can test it using:

```bash
sideview-profile-average -h
```

### Uninstalling

If we followed the instructions in this manual to install this package, we can just remove the `.nvm` directory to uninstall everything - including nodejs.

If we want to just uninstall `sideview-profile-average`, then just:

``` bash
npm -g uninstall sideview-profile-average
```

## Usage

So you want to use this package huh?! Ok, things are a little rough around here since I did not have the time to make it pretty and shinny.

Let's start from the basics:

This will make 1D averages from profiles that are collected perpendicular to a contour set in a specific slice of a tomogram.

Obviously, we will need a tomogram and a model file containing two contours: the first is the path where the profiles will be collected and the second one defines the plane in which the voxel value should be collected.

This is how we make paths using Imod:

1) switch from `Movie` to `Model` mode in the main panel.  
2) Now click on the slicer viewer and keep making model points.
3) to make a new `Countour` press Cmd+N (mac) or Ctrl+N (non-mac)
4) Click any two points to mark the plane of the tomogram we want to make the measurements.
5) Save model points: File > Save Model As

The model should be in data points (use `model2point` with the options `-ze` and `-ob`).

Example:

Let's suppose we saved the model above with the name `model.imod`.


``` bash
model2point -i model.imod -ou model.dat -ob -ze
```

We need to change the tomogram from `mrc` file format to `tif` stack using the `imod`'s `mrc2tif`. Use the option `-s` to stack all tiffs in a single file. Also, optional but recommended, use the option `-C` to rescale the voxel values - I tend to use the `black,white` values that the `3dmod` automatically pick. This might take a while for some tomograms:

``` bash
mrc2tif -s tomogram.rec tomogram.tif
```

With these two files, we can build the profiles.

## Building profiles

Now it comes the easy part. Just go to the directory where the two files above are.

There are only three more parameters we need to pass: step size, length and depth.

Step size is how many voxels between profiles as the software "walks through the path". I normally use 1 here.

Length is how long (in pixel) the profile extends perpendicular from the path.

Depth is how many slices perpendicular to the 2D view will be averaged. It is similar with the "thick: img" parameter in the `3dmod Slicer`.

Now that we defined how we want, just run:

``` bash
sideview-profile-average -t tomogram.tif -m model.dat -p 1 100 10
```

## Output file

The file sideview-profile-average is a JSON formated file with the raw data for each one of the profiles, some statistics and an average of all the profiles. Instead of explaining in details, I built a ObservableHQ to load and visualize the profile.

## Visualizing the profile

I haven't had the time to build a visualization tool. For now, we can use an Observable notebook for it:

[Generic notebook to analyse 1D averaged electron density profiles](https://beta.observablehq.com/@daviortega/generic-notebook-to-analyse-1d-averaged-electron-density-p)

We can upload right away and use without the need for login and whatnot. But if we want to increment the notebook and save the changes, we must have an account. It is free.

## Reporting bugs

If this script doesn't work for some reason, please, file a bug here:
[Bug report](https://gitlab.com/daviortega/sideview-profile-average/issues)

## Should I cite something?

We don't have a publication specific for this package, but if you feel like citing something, we first introduced and used it in:  

`Ortega, D. R. et al. Repurposing a chemosensory macromolecular machine. Nature Communications 11, 1–13 (2020).`

We explain the script in more detail in the supplementary material and we addressed a few questions about it in the review process (also available in the journal website.)

***

## TODO

- [ ] Cleanup code from unused packages
- [ ] Build and coverage badges
- [x] From console.log to bunyan logs
- [ ] Explaining output file format
- [ ] Test suit with simulated data
- [x] Performance improvement: Only load relevant parts of the tomogram
- [ ] Tutorial on how to make contour models in Imod

***
